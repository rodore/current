const path = require("path");

module.exports = {
  mode: "production",
  entry: {
    // app: "./src/app.js",
    main: "./src/main.js"
},
  output: {
      path: path.resolve(__dirname, "./public/dist"),
      filename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    //   {
    //   test: /\.js$/,
    //   exclude: /node_modules/,
    //   loader: "babel-loader"
    //  },
    ],
  },
};